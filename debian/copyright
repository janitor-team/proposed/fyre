Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fyre
Source: https://github.com/davidt/Fyre

Files: *
Copyright: 2004-2006 David Trowbridge and Micah Dowty
License: GPL-2+

Files: config.guess config.sub
Copyright: 1992-2006 Free Software Foundation, Inc.
License: GPL-2+

Files: debian/*
Copyright: 2004-2006, 2008 Christoph Haas
           2016, 2018, 2021 Stephen Kitt
License: GPL-2+

Files: src/curve-editor.c src/spline.c
Copyright: 1997 David Mosberger
License: GPL-2+

Files: src/curve-editor.h src/spline.h
Copyright: 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
License: GPL-2+

Files: src/getopt.c src/getopt1.c
Copyright: 1987-2002 Free Software Foundation, Inc.
License: LGPL-2.1+
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with the GNU C Library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in
 /usr/share/common-licenses/LGPL-2.1.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in /usr/share/common-licenses/GPL-2.
